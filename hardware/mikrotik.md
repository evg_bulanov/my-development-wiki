## Mikrotik

Для чего хакерам Микротик и как я спрятал 100 тыс. RouterOS от ботнета
https://habr.com/post/424433/


Уязвимость Mikrotik позволяет получать список всех пользователей через winbox
https://habr.com/post/359038/

Практика настройки Mikrotik для чайников
https://habr.com/post/265387/

4pda Маршрутизаторы MikroTik
https://4pda.ru/forum/index.php?showtopic=624146

Базовая настройка Mikrotik
https://www.youtube.com/watch?v=8YcC6tJUIeA

Как не работать ДНС если у вас есть Mikrotik
https://pikabu.ru/story/kak_ne_rabotat_dns_esli_u_vas_est_mikrotik_5062836


Создание домашней сети на базе устройств MikroTik: Часть 2 – Настройка hAP ac
https://gregory-gost.ru/sozdanie-domashney-seti-na-baze-ustroystv-mikrotik-part-2-nastroyka-hap-ac/