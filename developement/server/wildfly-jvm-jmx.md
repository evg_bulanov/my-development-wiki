## Visual VM for WildFly 10 (11, 12 ...)

Visual VM for WildFly 10 (11, 12 ...)

1. Start visual VM with jar from wildfly
   jvisualvm --cp:a /home/jk/servers/wildfly-itone-test/bin/client/jboss-client.jar

2. File -> Add JMX connection
3. Specify url, name, password, no ssl connection

service:jmx:remote+http://server:port

Note:
1. should be allowed to connect in standalone.xml
   <interfaces>
   <interface name="management">
   <any-address/>
   </interface>
   <interface name="public">
   <any-address/>
   </interface>
   </interfaces>

2. in the log file during start up there is a record about management listener
   2018-03-28 14:23:16,591 INFO  [org.jboss.as] (Controller Boot Thread) WFLYSRV0060: Http management interface listening on http://0.0.0.0:9990/management
   2018-03-28 14:23:16,592 INFO  [org.jboss.as] (Controller Boot Thread) WFLYSRV0051: Admin console listening on http://0.0.0.0:9990