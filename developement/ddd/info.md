Robert C Martin - Clean Architecture and Design
https://www.youtube.com/watch?v=lZq8Jlq18Ec

Domain-Driven Design: Tackling Complexity in the Heart of Software (Eric Evans)
Applying Domain-Driven Design And Patterns: With Examples in C# and .net (Jimmy Nilsson)
Implementing Domain-Driven Design (Vaughn Vernon)
Domain-Driven Design Distilled (Vaughn Vernon)

DDD in 10 mins
https://habr.com/ru/companies/dododev/articles/489352/

DOMAIN DRIVEN DESIGN 
https://mellarius.ru/heretic

Domain Driven Design Quickly
https://www.infoq.com/minibooks/domain-driven-design-quickly/


https://github.com/Sairyss/domain-driven-hexagon


## Многоликий DDD — Сергей Баранов
https://www.youtube.com/watch?v=NSN-NXfbEqM

## Алексей Мерсон — Domain-driven design: рецепт для прагматика
https://www.youtube.com/watch?v=CR9mLGN9jh0

## Владимир Хориков — Domain-driven design: Cамое важное
https://www.youtube.com/watch?v=JOy_SNK3qj4

## DDD books
https://github.com/GunterMueller/Books-3/blob/master/Domain%20Driven%20Design.pdf