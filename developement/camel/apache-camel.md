## Headers

Apache camel copies all headers to response and it should be clean up manually.
example:
```java
    message.removeHeaders("esirouting|operationname|x-api-key");
    
    message.setHeader("username", properties.getUser());
    message.setHeader("password", properties.getPassword());
    message.setHeader("api-key", properties.getToken());
```

it looks like this a flag "copyHeaders=false", but not well documented and did not work in my case. 

## mvcPattern or antMatchers (Spring boot security)

*Important:* **don't use mcvMatcher for apache camel REST** (antMatchers should be used instead)

mvcMatcher(String mvcPattern) - Allows configuring the HttpSecurity to only be invoked
when matching the provided Spring MVC pattern.