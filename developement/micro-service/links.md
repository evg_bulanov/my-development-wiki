## Helidon

Helidon is a collection of Java libraries for writing microservices that run on a fast web core powered by Netty.
https://helidon.io

## Prometheus

https://prometheus.io/
From metrics to insight
Power your metrics and alerting with a leading
open-source monitoring solution.

## istio (service mesh)

service mesh
istio.io

## tolerance (Failsafe, Hystrix)

### Failsafe
Failsafe is a lightweight, zero-dependency library for handling failures. 
It was designed to be as easy to use as possible, with a concise API for handling everyday 
use cases and the flexibility to handle everything else. Failsafe features:

### Hystrix: Latency and Fault Tolerance for Distributed Systems
https://github.com/Netflix/Hystrix

## zipkin

http://zipkin.io

Zipkin is a distributed tracing system. It helps gather timing data needed to troubleshoot latency 
problems in microservice architectures. It manages both the collection and lookup of this data. 
Zipkin’s design is based on the Google Dapper paper.

## REST video

Devoxx 2015 video
https://youtu.be/48azd2VqtP0?list=PLRsbF2sD7JVrnrqNopuT9GnkOSbFKtRvb
Resources as given in the video (https://youtu.be/48azd2VqtP0?t=3371) 
http://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api https://github.com/paypal/api-standards/blob/master/api-style-guide.md http://blog.octo.com/en/design-a-rest-api/ https://github.com/interagent/http-api-design/blob/master/SUMMARY.md http://sookocheff.com/post/api/on-choosing-a-hypermedia-format/ http://www.troyhunt.com/2014/02/your-api-versioning-is-wrong-which-is.html