Urls:

* https://javarevisited.blogspot.com/2021/09/microservices-design-patterns-principles.html#axzz7uXOPPPrZ
* https://medium.com/@madhukaudantha/microservice-architecture-and-design-patterns-for-microservices-e0e5013fd58a


## Database per Microservice

Two main options for organizing the databases:

* Database per service
* Shared database

### Database per service

The concept is straightforward. There is a data store for each microservice (whole schema or a table). 
Other services are unable to access data repositories that they do not control.

* Individual data storage -> is easy to scale. 
* understanding much easier

### Shared Database

Shared database is an anti-pattern. The issue is that when microservices use a shared database, 
they lose their key features of scalability, robustness, and independence.
A temporary state could be created while moving a monolith to microservices.

Transaction management is the fundamental advantage of a shared database versus a per-service database. 
There's no need to spread transactions out across services.

## Event Sourcing Pattern

Most applications work with data, and the typical approach is for the application to maintain the current state. 
For example, in the traditional create, read, update, and delete (CRUD) model a typical data process is to read data from the store. 
It contains limitations of locking the data with often using transactions.

The Event Sourcing pattern defines an approach to handling operations on data that’s driven by a sequence of events, 
each of which is recorded in an append-only store. Application code sends a series of events that imperatively describe 
each action that has occurred on the data to the event store, where they’re persisted. Each event represents a set of 
changes to the data (such as AddedItemToOrder).


The events are persisted in an event store that acts as the system of record.
Typical uses of the events published by the event store are to maintain materialized views of entities as actions in 
the application change them, and for integration with external systems. For example, a system can maintain a materialized 
view of all customer orders that are used to populate parts of the UI. As the application adds new orders, adds or 
removes items on the order, and adds shipping information, the events that describe these changes can be handled and 
used to update the materialized view. The figure shows an overview of the pattern.


![](https://miro.medium.com/v2/resize:fit:720/format:webp/0*iZ03fLMycI4iuQmv.png)

the image taken from https://medium.com/@madhukaudantha/microservice-architecture-and-design-patterns-for-microservices-e0e5013fd58a

## Command Query Segmentation (CQRS)  Pattern

* Commands - Change the state of the object or entity.
* Queries -  Return the state of the entity and will not change anything.

In traditional data management systems, there are some issues,

1. Risk of data contention
2. Managing performance and security is complex as objects are exposed to both reading and writing applications.

Benefits of using the CQRS:
1. The complexity of the system is reduced as the query models and commands are separated.
2. Can provide multiple views for query purposes.
3. Can optimize the read side of the system separately from the write side. 

![](https://1.bp.blogspot.com/-9NGW2zgCoeE/YUMhdQdpq7I/AAAAAAAAC6Q/Rixpf2gPswUlSHsVxEvhK74XolJQgvEGwCLcBGAsYHQ/w439-h238/cqsr_pattern-e1560327720529.png)

the image taken from https://javarevisited.blogspot.com/2021/09/microservices-design-patterns-principles.html#axzz7uXOPPPrZ

## Saga

https://medium.com/@madhukaudantha/microservice-architecture-and-design-patterns-for-microservices-e0e5013fd58a
https://medium.com/@kirill.sereda/saga-pattern-2daad957966e
https://medium.com/@madhukaudantha/microservice-architecture-and-design-patterns-for-microservices-e0e5013fd58a



Don’t start with a monolith
https://martinfowler.com/articles/dont-start-monolith.html
