### gradle

### How to Speed Up Your Gradle Build From 90 to 8 Minutes
https://dzone.com/articles/how-speed-your-gradle-build-90

### 10 Tips to Improve Your Gradle Build Time
http://www.universalmind.com/blog/10-tips-to-improve-your-gradle-build-time/

### Improving the performance of Gradle builds
https://gradle.github.io/performance-guide/