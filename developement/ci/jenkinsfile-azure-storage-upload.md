## Jenkinsfile azure storage upload 

```
pipeline {
    agent {
        docker {
            image 'node:8.9.4'
            args '-p 3000:3000'
        }
    }

    stages {
        stage('Node Build') {
            steps {
                sh 'npm install'
                sh 'npm install bower'
                sh './node_modules/.bin/bower install --allow-root'
                sh './node_modules/.bin/gulp build'
            }
        }
        stage('Upload files to azure') {
            steps {
                dir ('dist') {
                  azureUpload blobProperties: [cacheControl: '', contentEncoding: '', contentLanguage: '', contentType: '', detectContentType: true],
                  containerName: 'user-ui-${BUILD_NUMBER}', fileShareName: '', filesPath: '**/*.*', storageCredentialId: 'poctstorage', storageType: 'blobstorage'
                }
            }
        }
    }
}
```

