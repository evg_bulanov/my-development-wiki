## build best practices

### Fast building without tests:
./gradlew -x test --parallel --max-workers=3
max-workers - maximum number of threads that will be use in the build

### Full build:
./gradlew -PtaskThreads=2 -PforkEvery=20
taskThreads - use to limit CPU load during building
forkEvery - maximum number of test classes to execute in a forked test process, cause a new jvm to be forked for every 20 tests (hope it reduce total amount of memory that is used for tests)

### To build project without tests:
./gradlew build -x test --parallel --max-workers=<NUMBER_OF_THREADS>


### To speedup building process you could run build without test in parallel mode and then execute test sequentially:
./gradlew build -x test --parallel && ./gradlew build