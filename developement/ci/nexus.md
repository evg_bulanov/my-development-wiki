## Fix nexus database problems (corrupted data)

```shell

docker stop nexus
docker commit nexus test/nexus

docker run -it --user root  --entrypoint=/bin/bash -v nexus-data:/nexus-data test/nexus
rm /nexus-data/db/accesslog/*.wal
rm /nexus-data/db/security/*.wal
rm /nexus-data/db/audit/*.wal
rm /nexus-data/db/config/*.wal

java -jar /opt/sonatype/nexus/lib/support/nexus-orient-console.jar
connect plocal:/nexus-data/db/component/ admin admin
repair database --fix-links
rebuild index *
disconnect

connect plocal:/nexus-data/db/security/ admin admin
repair database --fix-links
rebuild index *
disconnect

connect plocal:/nexus-data/db/accesslog/ admin admin
repair database --fix-links
rebuild index *
disconnect

connect plocal:/nexus-data/db/audit/ admin admin
repair database --fix-links
rebuild index *
disconnect

connect plocal:/nexus-data/db/config/ admin admin
repair database --fix-links
rebuild index *

disconnect

exit

chown -R nexus:nexus /opt/sonatype/sonatype-work/nexus3/db/*
```