##jenkins url

### docker push
https://getintodevops.com/blog/building-your-first-docker-image-with-jenkins-2-guide-for-developers

### docker + azure
https://jenkins.io/blog/2017/08/10/kubernetes-with-pipeline-acs/

### azure upload with plugin
https://azure.microsoft.com/en-us/blog/annoucing-jenkins-deploy-to-azure-app-service-plugin-and-new-managed-disk-support-for-azure-storage-plugin/

### Azure App Service
https://plugins.jenkins.io/azure-app-service

### Pipeline Dependency Walker Plugin
https://wiki.jenkins.io/display/JENKINS/Pipeline+Dependency+Walker+Plugin

### Build a Java app with Maven
https://jenkins.io/doc/tutorials/build-a-java-app-with-maven/

### Получаем управление обратно в Jenkins Pipeline (форма с параметрами)
https://habrahabr.ru/post/302274/

### Использование хранилища Azure с решением непрерывной интеграции Jenkins (как вставить номер билда)
https://docs.microsoft.com/ru-ru/azure/storage/common/storage-java-jenkins-continuous-integration-solution