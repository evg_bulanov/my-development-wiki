## How to debug ssl handshake

Specify debug key -Djavax.net.debug=all to VM option and run

```java
// taken from https://github.com/dimalinux/SSLPing

public class SSLPing {
    public static void main(String[] args) {
        try {
            String hostname = "google.com";
            int port = 443;

            System.out.println("About to connect to '" + hostname + "' on port " + port);

            SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            SSLSocket sslsocket = (SSLSocket) sslsocketfactory.createSocket(hostname, port);

            // Hostname verification is not done by default in Java with raw SSL connections.
            // The next 3 lines enable it.
            SSLParameters sslParams = new SSLParameters();
            sslParams.setEndpointIdentificationAlgorithm("HTTPS");
            sslsocket.setSSLParameters(sslParams);

            // we only send 1 byte, so don't buffer
            sslsocket.setTcpNoDelay(true);

            // Write a test byte to trigger the SSL handshake
            OutputStream out = sslsocket.getOutputStream();
            out.write(1);

            // If no exception happened, we connected successfully
            System.out.println("Successfully connected");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```