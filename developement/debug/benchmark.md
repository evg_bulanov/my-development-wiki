## benchmark framework (JMH)

### JMH —great java benchmark framework.
https://medium.com/@dmitryegorov_20623/jhm-great-java-benchmark-framework-edd1e757c1e7

```
<dependency>
    <groupId>org.openjdk.jmh</groupId>
    <artifactId>jmh-core</artifactId>
    <version>1.0</version>
</dependency>
```