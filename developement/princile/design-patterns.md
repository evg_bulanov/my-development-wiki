# Design patterns

Design patterns are typical solutions to common problems  in software design. Each pattern is like a blueprint
that you can customize to solve a particular design problem in your code.

## Creation Design Patterns

### Singleton 

Singleton is a creational design pattern that lets you ensure that a class has only one instance, 
while providing a global access point to this instance.

![](https://refactoring.guru/images/patterns/content/singleton/singleton.png)

(the image above from refactoring.guru)

urls:
* https://refactoring.guru/design-patterns/singleton
* https://www.geeksforgeeks.org/java-singleton-design-pattern-practices-examples/
* https://dzone.com/articles/java-singletons-using-enum

Examples:

https://gitlab.com/evg_bulanov/some-algorithms-implementation-in-java/-/blob/main/src/main/java/some/inverview/questions/sngl/SingletonDemo.java

### Builder

Builder is a creational design pattern that lets you construct complex objects step by step. The pattern allows you to 
produce different types and representations of an object using the same construction code.

![](https://refactoring.guru/images/patterns/content/builder/builder-en.png)

(the image above from refactoring.guru)

urls:
 * https://refactoring.guru/design-patterns/builder
 * https://www.digitalocean.com/community/tutorials/builder-design-pattern-in-java

Examples: 

https://gitlab.com/evg_bulanov/some-algorithms-implementation-in-java/-/blob/main/src/main/java/some/inverview/questions/bld/BuilderDemo.java

### Prototype

Prototype is a creational design pattern that lets you copy existing objects without making your code dependent on their classes.

![](https://refactoring.guru/images/patterns/content/prototype/prototype.png)

(the image above from refactoring.guru)

urls:
* https://refactoring.guru/design-patterns/prototype
* https://www.baeldung.com/java-pattern-prototype
* 

Examples:

* The Prototype pattern is available in Java out of the box with a Cloneable interface.
* https://gitlab.com/evg_bulanov/some-algorithms-implementation-in-java/-/blob/c047dcfa10301ee76021fc20f68264cf293d4a5d/src/main/java/some/inverview/questions/prt/PrototypeDemo.java

### Factory Method

Factory Method is a creational design pattern that provides an interface for creating objects in a superclass, but 
allows subclasses to alter the type of objects that will be created.

Factory Method: A class or an interface relies on a derived class to provide the implementation 
whereas the base provides common behavior

Factory method uses inheritance as a design tool. 

![](https://refactoring.guru/images/patterns/content/factory-method/factory-method-en.png)

(the image above from refactoring.guru)

Examples:

https://gitlab.com/evg_bulanov/some-algorithms-implementation-in-java/-/blob/main/src/main/java/some/inverview/questions/fctr/mthd/FactoryMethodDemo.java

https://gitlab.com/evg_bulanov/some-algorithms-implementation-in-java/-/blob/main/src/main/java/some/inverview/questions/fctr/mthd/FactoryMethodAsDefaultDemo.java


The pattern is present in core Java libraries:

* java.util.Calendar#getInstance()
* java.util.ResourceBundle#getBundle()
* java.text.NumberFormat#getInstance()
* java.nio.charset.Charset#forName()
* java.net.URLStreamHandlerFactory#createURLStreamHandler(String) (Returns different singleton objects, depending on a protocol)
* java.util.EnumSet#of()
* javax.xml.bind.JAXBContext#createMarshaller() and other similar methods.

* Identification: Factory methods can be recognized by creation methods that construct objects 
* from concrete classes. While concrete classes are used during the object creation, the return type 
* of the factory methods is usually declared as either an abstract class or an interface.

### Abstract Factory

Abstract Factory is a creational design pattern that lets you produce families of related objects without specifying their concrete classes.

![](https://refactoring.guru/images/patterns/content/abstract-factory/abstract-factory-en.png)

(the image above from refactoring.guru)

Examples in java:

https://gitlab.com/evg_bulanov/some-algorithms-implementation-in-java/-/blob/main/src/main/java/some/inverview/questions/fctr/abstr/AbstractFactoryMethodDemo.java

* javax.xml.parsers.DocumentBuilderFactory#newInstance()
* javax.xml.transform.TransformerFactory#newInstance()
* javax.xml.xpath.XPathFactory#newInstance()

## Structural Design Patterns

### Adapter

Adapter is a structural design pattern that allows objects with incompatible interfaces to collaborate.

![](https://refactoring.guru/images/patterns/content/adapter/adapter-en.png)

(the image above from refactoring.guru)


Examples

https://gitlab.com/evg_bulanov/some-algorithms-implementation-in-java/-/blob/main/src/main/java/some/inverview/questions/adptr/AdaptorDemo.java

The Adapter pattern is pretty common in Java code. It’s very often used in systems based on some legacy code. 
In such cases, Adapters make legacy code work with modern classes.

There are some standard Adapters in Java core libraries:

* java.util.Arrays#asList()
* java.util.Collections#list()
* java.util.Collections#enumeration()
* java.io.InputStreamReader(InputStream) (returns a Reader object)
* java.io.OutputStreamWriter(OutputStream) (returns a Writer object)
* javax.xml.bind.annotation.adapters.XmlAdapter#marshal() and #unmarshal()

### Bridge

Bridge is a structural design pattern that lets you split a large class or a set of closely related classes into 
two separate hierarchies—abstraction and implementation—which can be developed independently of each other.
hierarchies: abstractions and implementations.

divides and organizes a single class that has multiple variants of some functionallity into two 

![](https://refactoring.guru/images/patterns/content/bridge/bridge.png)

(the image above from refactoring.guru)

https://www.youtube.com/watch?v=88kAIisOiYs
https://refactoring.guru/design-patterns/bridge

Examples

https://gitlab.com/evg_bulanov/some-algorithms-implementation-in-java/-/blob/5769756ea41b9e438c042cb424b9bf90b1821af1/src/main/java/some/inverview/questions/brdg/BridgeDemo.java

