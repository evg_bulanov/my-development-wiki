Core Design Principles for Software Developers by Venkat Subramaniam
https://www.youtube.com/watch?v=llGgO74uXMI

Topic
* KISS (keep it simple and stupid)
* YAGNIy (you are not gonna need it yet)
* Cohesion / сплоченность
* Coupling
* DRY
* Focus on Single Responsibility
* SLAP (Single Level of Abstraction)
* Compose Method Pattern
* OCP / Don't violate the Open-Closed Principle
* Liskov's Substitution Principle
* Decouple using the Dependency Inversion Principle
* Keep Interfaces Cohesive with Interface Segregation Principle
* SOLID = SRP, OCP, LSP, ISP, DIP


Good design - cost of changing a design is minimum (easy to change)

Almost impossible to get it right the first time!!!

Software is never written, it is always rewritten!

To create good design first step is let go of the ego (be unemotional)

There are two kinds of people that are dangerous to work
1. Who can't follow  instructions
2. Who can only follow instructions

Take time to review design and code

## KISS keep it simple and stupid

Simple keeps you focused
Simple solves only real problem we know about
Sample fails less
Simple is easy to understand

Complexity:
* inherit complexity (from domain you can't do anything with it)
* accidental complexity - complexity of a solution

A good design is the one that hide inherent complexity and eliminates
the accidental complexity.

## YAGNIy (you are not gonna need it yet)

How much do you know  / cost of implementing: 
> now      /         later                      \
> $N       >         $L - postpone              \
> $N       =         $L - postpone              \
> $N       >         $L                         \

- how probable?
> high - do it now
> low  - postpone

Manual testing
end lets change - are you crazy o_O

Automated testing
end lets change - give it try ;)

> If you want to postpone we need to good automated testing.

## Cohesion / сплоченность

Narrow, focused and does one thing and one thing well!

We want software to change, but not too expensive

If a code is cohesive, it has to change less frequently


## Coupling

Coupling what you depend on

Worst form of coupling - Inheritance

Try to see if you can remove coupling

"knock out before you mock out"

Can't remove all the dependencies

1. Get rid of it
2. Make it loose instead of tight

Depending on a class is tight coupling
Depending on an interface is loose coupling

A good design has high cohesion and low coupling


## DRY

Don't Repeat Yourself

Don't duplicate - code and also **effort**

Every piece of knowledge in a system should have
a single and unambiguous authoritative representation.

It reduces the cost of development!


## Focus on Single Responsibility

Antipattern: this class does this and ... , this method does this and ...

Long method pattern: hard to test, hard to read, hard to remember, obscured to rules,
hard to reuse, leads to duplication, many reasons to changes, can't optimize by anything,
mixed levels, lack cohesion!, high coupling!, hard to debug.
> // comment                \
> ..                        \
> // comment                \
> ..                        \
> ...                        
> // comment                


## SLAP (Single Level of Abstraction)

Only keep the code in the same level of abstraction
if you don't understand code -> refactor it, don't comment

Don't comment **what**, instead comment **why**

## Compose Method Pattern

The code should be composed of step that you wanna take in the logic.
step1, step2, step3 -> self documented code.

## OCP / Don't violate the Open-Closed Principle

Software module should be open for extension and closed from modification.
Abstraction and polymorphism are the key to make this happen.

Polymorphism -  the ability of a class to provide different implementations of a method, 
depending on the type of object that is passed to the method.

If you make your code infinite extendable it increases complexity.
(We need to know software and domain).

Advice don't implement it first time, find domain expert and about planning extension
it this area. **Apply YAGNIy principle first! (before applying OCP)**

Ex: you have a Car class and have to modify it every time you add a new type
of Engine class.

## Liskov's Substitution Principle

Inheritance overused, it should be used only for substitutability.

If an object of B should be used anywhere an object of A is used then use inheritance.
If an object of B should use an object of A, then ues composition / contain / delegation.


Services of the derived class **should require no more and promise no less** than the
corresponding services of the base class.

Why?

The user of a base class should be able to use an instance of derived class
without knowing the difference.

Good

public vs protected in base vs. derived 
derived function can't throw any new checked exception not thrown by the base
(unless the new exception extends the old one)
Collection of derived doest not extend from collection of base

Bad

Example Stack.class.

Use composition or delegation instead of inheritance unless you want substitutability.
But for some cases
if you use inheritance you violate LSP
if you use delegation you violate DRY, OCP
It is better to violate DRY and OCP (otherwise your users violate them)


## Decouple using the Dependency Inversion Principle

A class should not depend on another class, they both have to depend
on an abstraction (interface).

Use caution!

Venkat -X->  Clock
--->  Alarm (Functional interface)
^
|
-----------------------
|    |     |   ....   |
TV  Comp  SPhone      Wife

Instead of a person should depend on a concrete implementation of clock,
but both classes (Person and Clock) should depend on Alarm.
It lowers coupling in the code (becomes easy to extend but increases complexity).

## Keep Interfaces Cohesive with Interface Segregation Principle

Cohesion on interface level (don't make them fat, instead make them really cohesive,
focused on one thing)

## SOLID

Single Responsibility (SRP)
Open-Closed Principle (OCP)
Liskov's Substitution Principle (LSP)
Interface Segregation Principle (ISP)
Dependency Inversion Principle (DIP)
