## Different Types of References in Java

* Strong Reference
* Weak Reference
* Soft Reference
* Phantom Reference

## Strong Reference:  

We use Strong references in Java everywhere: we can create an object and then assign it to a reference. 

## Soft Reference

If an object has no strong reference but has a soft reference, then the garbage collector reclaims this object’s 
memory when GC needs to free up some memory. To get Object from a soft reference, one can invoke the get() method. 
If the object is not GCed, it returns the object, otherwise , it returns null.

Soft reference objects, which are cleared at the discretion of the garbage collector in response to memory demand. 
Soft references are most often used to implement memory-sensitive caches.

## Weak Reference

If an object has no strong reference but has a weak reference then GC reclaims this object’s 
memory in next run even though there is enough memory.

Weak reference objects, which do not prevent their referents from being made finalizable, 
finalized, and then reclaimed.

## Phantom Reference

If an object does not have any of the above references then it may have a phantom reference. 
Phantom references can’t be accessed directly. When using a get() method it will always return null. 

Phantom reference objects, which are enqueued after the collector determines that their referents may otherwise be reclaimed. 
Phantom references are most often used to schedule post-mortem cleanup actions.

## Final Reference

Final references, used to implement finalization

```java

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

public class ReferenceExample {
    private String status = "Hi I am active";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ReferenceExample [status=" + status + "]";
    }

    public void strongReference() {
        ReferenceExample ex = new ReferenceExample();
        System.out.println(ex);
    }

    public void softReference() {
        SoftReference<ReferenceExample> ex = new SoftReference<ReferenceExample>(getReference());
        System.out.println("Soft refrence :: " + ex.get());
    }

    public void weakReference() {
        int counter = 0;
        WeakReference<ReferenceExample> ex = new WeakReference<ReferenceExample>(getReference());
        while (ex.get() != null) {
            counter++;
            System.gc();
            System.out.println("Weak reference deleted  after:: " + counter + ex.get());
        }
    }

    public void phantomReference() throws InterruptedException {
        final ReferenceQueue queue = new ReferenceQueue();
        PhantomReference<ReferenceExample> ex = new PhantomReference<ReferenceExample>(getReference(), queue);
        System.gc();
        queue.remove();
        System.out.println("Phantom reference deleted  after");
    }

    private ReferenceExample getReference() {
        return new ReferenceExample();
    }

    public static void main(String[] args) {
        ReferenceExample ex = new ReferenceExample();
        ex.strongReference();
        ex.softReference();
        ex.weakReference();
        try {
            ex.phantomReference();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```

The info taken from DZone and java source code

https://dzone.com/articles/java-different-types-of-references