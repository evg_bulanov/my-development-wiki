## git

### to get rid of the identical files during merging:
git merge master -s recursive -Xignore-all-space

### fix hanging of pushing on Windows:
git config --global sendpack.sideband false

### info about a commit 
git cat-file -p 92384bd

### create a repo
http://qugstart.com/blog/ruby-and-rails/create-a-new-git-remote-repository-from-some-local-files-or-local-git-repository/
http://stackoverflow.com/questions/6648995/how-to-create-a-remote-git-repository-from-a-local-one/20987150#20987150

don't forget to create file: git-daemon-export-ok to access the repo