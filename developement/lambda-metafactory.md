## Java lambda / lambda metafactory

### Hacking Lambda Expressions in Java
https://dzone.com/articles/hacking-lambda-expressions-in-java

### Разбираем лямбда-выражения в Java
https://habr.com/company/haulmont/blog/432418/

### Что такое Method Handles в Java
https://habr.com/company/haulmont/blog/431922/

### 7 способов использовать groupingBy в Stream API
https://habr.com/post/348536/

### LambdaMetafactory:
https://docs.oracle.com/javase/8/docs/api/java/lang/invoke/LambdaMetafactory.html
https://stackoverflow.com/questions/26775676/explicit-use-of-lambdametafactory
https://www.optaplanner.org/blog/2018/01/09/JavaReflectionButMuchFaster.html