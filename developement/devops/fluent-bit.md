## Install fluent-bit

wget -qO - http://packages.fluentbit.io/fluentbit.key | sudo apt-key add -
sudo add-apt-repository 'deb http://packages.fluentbit.io/ubuntu xenial main'
sudo apt-get update
sudo apt-get install td-agent-bit

sudo service td-agent-bit start

to check:
sudo tail -f /var/log/syslog
sudo service td-agent-bit status

Edit config:
/etc/td-agent-bit/td-agent-bit.conf

config sample:
https://raw.githubusercontent.com/fluent/fluent-bit-kubernetes-logging/master/output/elasticsearch/fluent-bit-configmap.yaml