## docker commands

### Remove containers by name (rancher)
docker rm -f $(docker ps -a |grep rancher |awk ‘{print $1}’)

### Remove images by name (rancher)
docker rmi -f $(docker images | grep rancher)

### Remove dangling volumes
docker volume ls -qf dangling=true | xargs -r docker volume rm

### Delete all stopped containers
docker ps -q -f status=exited | xargs --no-run-if-empty docker rm

### Delete all dangling (unused) images
docker images -q -f dangling=true | xargs --no-run-if-empty docker rmi

### How To Remove Docker Images, Containers, and Volumes
https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes