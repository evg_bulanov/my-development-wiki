## QA management tools 

Top Free Test Case Management Tools List
https://blog.testlodge.com/free-test-case-management-tools-list/

Test management is a complex world. It involves a lot of coordination, communication, documentation and tooling. 
Test case management tools exist to improve the lives of software testers. Free test case management tools are 
often good alternatives to paying for licenses, and they can certainly be good alternatives to using a traditional 
spreadsheet for managing tests.

Free test case management tools

No two teams are identical, and everyone has their own way of working. A good test case management tool is simple 
to use, promotes collaboration, and allows for some flexibility. The right tool can have a major and positive 
impact on you and your team, leading to better test coverage, more efficient testing, and better reporting.

In a world of paid SaaS products, it’s easy to forget there are other options available. In this article, 
we’ve included a list of free test case management tools and a quick description of each.

Free Test Case Management Tools:
Testopia
qaManager
Tarantula
TestLink
Kiwi TCMS
TestMaster
WebTst
Salome-TMF


Further details of each tool:

1. Testopia (A Bugzilla extension)
   Test case extension Testopia

Testopia is an extension for Bugzilla that allows for basic test case management. With Testopia, you can integrate 
bug reporting with the results of a test run. They also allow you to export test cases and results. 
With Testopia, users can login to one tool and use the Bugzilla group permissions to control permissions 
and access for modifying the tests.

Link: Testopia

2. qaManager
   free test management qaManager logo

qaManager is a free test case management web-based tool. It is an application for managing both testing projects 
and testing teams. Features include the ability to track releases, manage test cases, resource allocation, 
track code reviews, and reports.

Link: qaManager

3. Tarantula
   Agile test management Tarantula

Tarantula is an open-source, self-hosted tool for managing agile software testing projects. Tarantula allows 
testers to quickly spin up new test executions based on tags and smart tags applied to test cases. It also offers 
an easy to use interface and reporting tool so project managers and test managers can stay in the loop on how the 
testing process is going. Tarantula is a free test case management tool licensed as open source software.

It is worth knowing that the company who developed this tool have mentioned that support activity is not a 
high priority for them.

Link: Tarantula

4. TestLink
   Open source test management TestLink

Perhaps one of the more popular free test case management tools available today, TestLink is a great free 
alternative to using spreadsheets for test case management or paying for a service. This web-based tool allows 
you to import and export test cases, offers various user roles, and supports manual and automated test execution. 
TestLink also integrates with many popular issue tracking tools, allowing you to link test cases to bug tickets.

Link: TestLink

5. Kiwi TCMS
   Kiwi TCMS

A more recent addition to the open source test case management tools available, Kiwi TCMS provides another 
good alternative for people looking to host a test management tool themselves. Along with a good variety of 
features, Kiwi TCMS provides an API, good user access controls along with some basic reporting. Built using Python,
Kiwi TCMS comes bundled with a Docker image making life a little easier when it comes to setting up the server 
to host the system.

Link: Kiwi TCMS

6. TestMaster
   Test case Testmaster

TestMaster is a web-based tool born from the need to report on which test cases are complete/incomplete. 
It offers email reports, importing of test cases from word docs and CSV files, search, and basic reporting. 
TestMaster runs on Linux and uses Apache to serve web pages. TestMaster is a great alternative to managing your 
testing projects in a spreadsheet.

Link: TestMaster

7. WebTst
   WebTst

WebTst is an open source tool that can be used to write simple automated test scripts for both functional 
and unit testing. It is a Java app that runs everywhere as long as you have a JDK for your operating system. 
The recorder allows you to quickly create a rough draft of your tests.

Link: WebTst

8. Salome-TMF
   Salome TMF logo

Salome-TMF is a free test case management tool that supports the creation of manual tests and automated tests. 
You can define multiple environments for running your tests and track the results of those tests under each 
environment. Salome-TMF helps you organize your test cases in a hierarchical tree structure.