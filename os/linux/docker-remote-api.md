## Without secure
Find out docker config
* systemctl cat docker.service
* Remove in /lib/systemd/system/docker.service '-H unix://'
ExecStart=/usr/bin/dockerd -H unix://
* Extend /etc/docker/daemon.json
"hosts": [“unix:///var/run/docker.sock”, “tcp://0.0.0.0:2376”],
* Restart

```shell
sudo systemctl daemon-reload
sudo service docker start
```

### To check:
```shell
sudo netstat -lntp | grep dockerd
curl http://localhost:2376/version
```

### useful links:
https://gist.github.com/kekru/b9e4da822514df93e6fdf2f7d3d90d8a

### With SSL
https://docs.docker.com/engine/security/https/#create-a-ca-server-and-client-keys-with-openssl