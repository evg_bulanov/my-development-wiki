## docker secrets script

```dockerfile
#!/usr/bin/env bash
set -e

# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
local var="$1"
local fileVar="${var}_FILE"
local def="${2:-}"
if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
exit 1
fi
local val="$def"
if [ "${!var:-}" ]; then
val="${!var}"
elif [ "${!fileVar:-}" ]; then
val="$(< "${!fileVar}")"
fi
export "$var"="$val"
unset "$fileVar"
}

file_env 'MY_TEST'
echo $MY_TEST
exec java -jar /app/app.jar


docker file

FROM openjdk:8-jdk-alpine
VOLUME /tmp
RUN apk add --no-cache bash
RUN mkdir /app
ADD build/libs/demo-0.0.1-SNAPSHOT.jar /app/app.jar

ADD etc/config/docker/docker-entry-point.sh /usr/local/bin/docker-entry-point.sh
RUN chmod +x /usr/local/bin/docker-entry-point.sh
RUN echo 'my_secret_password' > /app/test

# Define default command.
ENTRYPOINT [ "/usr/local/bin/docker-entry-point.sh" ]
```
