### How to find where to many inodes occupied
```shell
find . -xdev -type f | cut -d "/" -f 2 | sort --buffer-size=8G| uniq -c | sort -n --buffer-size=8G
```
