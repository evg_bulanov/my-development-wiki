settings for idea

/etc/sysctl.conf
fs.inotify.max_user_watches = 3000000
fs.file-max = 5000000

/etc/security/limits.conf (restart)
* hard nofile 65535
* soft nofile 65535

The One and Only Reason to Customize IntelliJ IDEA Memory Settings

https://dzone.com/articles/the-one-and-only-reason-to-customize-intellij-idea?edition=120053&utm_source=Daily%20Digest&utm_medium=email&utm_content=Daily_Digest_A3&utm_campaign=dd%202015-11-30&userid=529459
