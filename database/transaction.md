## Transaction & Transaction Propagation

Basic info could be got from here https://www.baeldung.com/spring-transactional-propagation-isolation

In spring or java ee annotation @Transactional

### Transaction Propagation

* REQUIRED
* SUPPORTS
* MANDATORY
* NEVER
* NOT_SUPPORTED
* REQUIRES_NEW 
* NESTED

### Transaction Isolation

Isolation is one of the common ACID properties: Atomicity, Consistency, Isolation, and Durability. 
Isolation describes how changes applied by concurrent transactions are visible to each other.

* A - Atomicity
* C - Consistency
* I - Isolation
* D - Durability

Each isolation level prevents zero or more concurrency side effects on a transaction:

* **Dirty read**: read the uncommitted change of a concurrent transaction
* **Nonrepeatable** read: get different value on re-read of a row if a concurrent transaction updates the same row and commits
* **Phantom read**: get different rows after re-execution of a range query if another transaction adds or removes some rows in the range and commits