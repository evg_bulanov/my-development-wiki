## How to unlock oracle user 

```sql
select USERNAME,ACCOUNT_STATUS from dba_users where username='USER_NAME';

alter user USER_NAME account unlock;

ALTER USER USER_NAME IDENTIFIED BY USER_PASSWORD;
```

## If oracle in docker container

```shell
docker exec -it oracle-19-server /bin/bash

sqlplus / as sysdba

alter profile default limit password_life_time unlimited;

alter user USER_NAME identified by USER_NAME account unlock;

```
