## How to connect to postgres in docker container

```shell
docker exec -it <docker_container_name> psql -d <db_name> -U <user_name> 
```
