## JWT, JWS, JWE

JWT debuger
https://jwt.io/

JAX-RS Security using JSON Web Encryption (JWE) with JWS/JWT for Authentication and Authorization
https://avaldes.com/jax-rs-security-using-json-web-encryption-jwe-with-jwsjwt-for-authentication-and-authorization/

Build a REST API Using Java, MicroProfile, and JWT Authentication
https://developer.okta.com/blog/2019/07/10/java-microprofile-jwt-auth

THE HARD PARTS OF JWT SECURITY NOBODY TALKS ABOUT
https://www.pingidentity.com/fr/company/blog/posts/2019/the-hard-parts-of-jwt-security-nobody-talks-about.html