## Some useful link for re-engineering user sessions

OAuth 2.0 for Browser-Based Apps
https://datatracker.ietf.org/doc/html/draft-ietf-oauth-browser-based-apps#name-single-domain-browser-based

https://www.marcobehler.com/guides/spring-security
https://www.innoq.com/en/blog/cookie-based-spring-security-session/
https://redis.com/blog/json-web-tokens-jwt-are-dangerous-for-user-sessions/
https://stackoverflow.com/questions/43452896/authentication-jwt-usage-vs-session
https://www.rdegges.com/2018/please-stop-using-local-storage/

How JWT Authentication Works
https://docs.spring.io/spring-security/reference/servlet/oauth2/resource-server/jwt.html