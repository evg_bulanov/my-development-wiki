## How to establish login by ssh with rsa

```shell
ssh-keygen -t rsa
ssh-copy-id -i ~/keys/my-key-id-rsa user@10.10.10.10
ssh -i my-key-id-rsa user@10.10.10.10
```
