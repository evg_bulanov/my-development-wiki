## Spring boot security

https://www.youtube.com/watch?v=mD3vmgksvz8

https://www.youtube.com/watch?v=-HYrUs1ZCLI

### Managing Secrets with Vault
https://spring.io/blog/2016/06/24/managing-secrets-with-vault

### Username and password encryption in Spring boot application properties file
https://happilyblogging.wordpress.com/2017/08/30/username-and-password-encryption-in-spring-boot-application-properties-file/

### Spring Boot: Encrypt Property Value in Properties File
http://www.ru-rocker.com/2017/01/13/spring-boot-encrypting-sensitive-variable-properties-file/

### Managing Secrets With Vault
https://dzone.com/articles/managing-secrets-with-vault